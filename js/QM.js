/**
 * Created by Administrator on 2017/2/24.
 */
$(function () {
	// 主页开始
	var box1 = $('.box1');
	var box1_h = box1.height();
	var box1_first_carousel;
	var box1_last_carousel;
	setInterval(function () {
		box1_first_carousel = box1.find('.qm-carousel').first();
		box1_last_carousel = box1.find('.qm-carousel').last();
		box1_first_carousel.animate({
			marginTop: -box1_h,
			opacity: 0
		}, 'slow', function () {
			box1_first_carousel.css({
				'margin-top': 0,
				'opacity': 1
			}).insertAfter(box1_last_carousel)
		})
	}, 3000);

	var box2 = $('.box2');
	var box2_h = box2.height();
	var box2_first_carousel;
	var box2_last_carousel;
	setInterval(function () {
		box2_first_carousel = box2.find('.qm-carousel').first();
		box2_last_carousel = box2.find('.qm-carousel').last();
		box2_first_carousel.animate({
			marginTop: -box2_h,
			opacity: 0
		}, 'slow', function () {
			box2_first_carousel.css({
				'margin-top': 0,
				'opacity': 1
			}).insertAfter(box2_last_carousel)
		})
	}, 3000);
	// 主页结束

	// 需求大厅开始
	var dhBox1 = $('.dhBox1');
	var dhBox1_h = dhBox1.height();
	var dhBox1_first_carousel;
	var dhBox1_last_carousel;
	setInterval(function () {
		dhBox1_first_carousel = dhBox1.find('.qm-carousel').first();
		dhBox1_last_carousel = dhBox1.find('.qm-carousel').last();
		dhBox1_first_carousel.animate({
			marginTop: -dhBox1_h,
			opacity: 0
		}, 'slow', function () {
			dhBox1_first_carousel.css({
				'margin-top': 0,
				'opacity': 1
			}).insertAfter(dhBox1_last_carousel)
		})
	}, 3000);

	var dhBox2 = $('.dhBox2');
	var dhBox2_h = dhBox2.height();
	var dhBox2_first_carousel;
	var dhBox2_last_carousel;
	setInterval(function () {
		dhBox2_first_carousel = dhBox2.find('.qm-carousel').first();
		dhBox2_last_carousel = dhBox2.find('.qm-carousel').last();
		dhBox2_first_carousel.animate({
			marginTop: -dhBox2_h,
			opacity: 0
		}, 'slow', function () {
			dhBox2_first_carousel.css({
				'margin-top': 0,
				'opacity': 1
			}).insertAfter(dhBox2_last_carousel)
		})
	}, 3000);

	var dhBox3 = $('.dhBox3');
	var dhBox3_h = dhBox3.height();
	var dhBox3_first_carousel;
	var dhBox3_last_carousel;
	setInterval(function () {
		dhBox3_first_carousel = dhBox3.find('.qm-carousel').first();
		dhBox3_last_carousel = dhBox3.find('.qm-carousel').last();
		dhBox3_first_carousel.animate({
			marginTop: -dhBox3_h,
			opacity: 0
		}, 'slow', function () {
			dhBox3_first_carousel.css({
				'margin-top': 0,
				'opacity': 1
			}).insertAfter(dhBox3_last_carousel)
		})
	}, 3000);
	// 需求大厅结束


	// 服务商库开始
	var $categoryRowBody = $('.category-row .row-body');

	$categoryRowBody.each(function (index, item) {
		console.log($(item).height());
		if ($(item).height() === 50) {
			$(item).next('.more-btn').html('');
		} else {
			$(item)
				.next('.more-btn')
				.find('a')
				.on('click', function (e) {
					e.preventDefault();
					e.stopPropagation();

					var $categoryRow = $(item).parent('.category-row');
					if ($categoryRow.hasClass('expand-mode')) {
						$(this).html('更多<i class="triangle triangle-down"></i>');
						$categoryRow.removeClass('expand-mode');
					} else {
						$categoryRow.addClass('expand-mode');
						$(this).html('收起<i class="triangle triangle-up"></i>');
					}
				});
		}
	})
});

