(function () {
  // 多行省略
  var arrLine4 = document.getElementsByClassName('text-overflow-hidden-nowrap-line4'),
      arrLine3 = document.getElementsByClassName('text-overflow-hidden-nowrap-line3');
      arrLine2 = document.getElementsByClassName('text-overflow-hidden-nowrap-line2');
  for (var i=0; i<arrLine4.length; i++) {
      $clamp(arrLine4[i], {clamp: 4});
  }
    for (var i=0; i<arrLine3.length; i++) {
        $clamp(arrLine3[i], {clamp: 3});
    }
    for (var i=0; i<arrLine2.length; i++) {
        $clamp(arrLine2[i], {clamp: 2});
    }
}());